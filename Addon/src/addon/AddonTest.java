package addon;

public class AddonTest {

	public static void main(String[] args) {
	Addon addon1 = new Addon();
	
	addon1.setBezeichnung("LvlBooster");
	addon1.setPreis(1.59);
	addon1.setIdNummer(12345);
	addon1.setMaxAnzahl(10);
	addon1.setBesitzt(1);
	
	System.out.println("Bezeichnung: "+ addon1.getBezeichnung());
	System.out.println("Preis: "+addon1.getPreis());
	System.out.println("IdNummer: "+addon1.getIdNummer());
	System.out.println("MaxAnzahl: "+addon1.getMaxAnzahl());
	System.out.println("Besitzt: "+addon1.getBesitzt());
}

	
}