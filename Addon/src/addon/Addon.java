package addon;

public class Addon {

	
//Attribute
	private String bezeichnung;
	private double preis; 
	private int idNummer; 
	private int besitzt; 
	private int maxAnzahl; 
	private int erfolg;
	private double gesamtwert;
//Konstruktor

	public Addon(){
	}
	
//Methoden
	public void setBezeichnung(String bezeichnung){
		this.bezeichnung = bezeichnung;
	}
	public String getBezeichnung(){
		return bezeichnung;
	}
	public void setPreis(double preis){
		this.preis = preis;
	}
	public double getPreis(){
		return preis;
	}
	public void setIdNummer(int idNummer){
		this.idNummer = idNummer;
	}
	public int getIdNummer(){
		return idNummer;
	}
	public void setBesitzt(int besitzt){
		this.besitzt = besitzt;
	}
	public int getBesitzt(){
		return besitzt;
	}
	public void setMaxAnzahl(int maxAnzahl){
		this.maxAnzahl = maxAnzahl;
	}
	public int getMaxAnzahl(){
		return maxAnzahl;
	}
	public int  kaufen(int besitzt){
		if (besitzt<maxAnzahl){
			this.besitzt = besitzt +1;
		}
		return besitzt;
		
	}
	public int  verbrauchen(int besitzt){
		this.besitzt = besitzt -1;
		return besitzt;
	}
	public void setGesamtwert(double gesamtwert){
		this.gesamtwert= preis*besitzt;
	}
	public double getGesamtwert(){
		return gesamtwert;
	}
	public int  freiErfolg(int erfolg){
		if (gesamtwert >=50){
			erfolg = erfolg + 1;
		}
		return erfolg;
	}
	
}

